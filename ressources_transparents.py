# -*- coding: utf-8 -*-

import dtw
import mfcc
import traitementsignal

import audio_record
import numpy
import time

from pylab import *
from matplotlib import gridspec

from scipy.io import wavfile as wav

import matplotlib.pyplot as plt
from scipy.io import wavfile as wav
from scipy.fftpack import fft
import numpy as np

from scipy import stats

import sys  
reload(sys)  
sys.setdefaultencoding('utf8')

# SIGNAL

#rate, data = wav.read("Enregistrements/Demos/Trois/demoTrois24.wav")
#rate, data = wav.read("Enregistrements/Demos/Neuf/demoNeuf15.wav")
#
##signal = np.array(data[27000:37500], dtype=float)*100 / max(data[27000:37500])
#signal = np.array(data[21800:34500], dtype=float)*100 / max(data[21800:34500])
#time = ( numpy.arange(0, float(len(signal)), 1) / rate1 ) * 1000
#
#plt.ylabel('Intensité (en %)')
#plt.plot(time, signal, 'b-')
#
#savefig('foo.png')




# DECOUPAGE DU SIGNAL

#Trois = wav.read("Enregistrements/Demos/Trois/demoTrois24.wav")
#
#gs = gridspec.GridSpec(2, 2)
#fig = plt.figure()
#
#signal = np.array(Trois[1][27000:37500], dtype=float)*100 / max(Trois[1][27000:37500])
#calc = mfcc.decoupageSignal(signal1, 0.025*Trois[0], 0.01*Trois[0])
#
#fen1 = calc[5]
#fen2 = calc[10]
#
#time1 = ( numpy.arange(0, float(len(signal)), 1) / Trois[0] ) * 1000
#time2 = ( numpy.arange(0, float(len(fen1)), 1) / Trois[0] ) * 1000
#time3 = ( numpy.arange(0, float(len(fen2)), 1) / Trois[0] ) * 1000
#
#ax1 = fig.add_subplot(gs[0,:])
#plt.ylabel('Intensité (en %)')
#ax1.set_title("Signal entier")
#ax1.plot(time1, signal, 'r-')
#
#ax2 = fig.add_subplot(gs[1,0])
#ax2.set_title("Fenêtre 5")
#plt.ylabel('Intensité (en %)')
#plt.xlabel('Temps (ms)')
#ax2.plot(time2, fen1, 'b-')
#
#ax3 = fig.add_subplot(gs[1,1])
#ax3.set_title("Fenêtre 10")
#plt.xlabel('Temps (ms)')
#ax3.plot(time3, fen2, 'b-')
#
#savefig('foo.png')




# HAMMING

#Trois = wav.read("Enregistrements/Demos/Trois/demoTrois24.wav")
#
#gs = gridspec.GridSpec(1, 2)
#fig = plt.figure()
#
#signal = Trois[1][27000:37500]
#calc = traitementsignal.framesig(signal,0.025*Trois[0],0.01*Trois[0])
#window = calc[7] * 100 / max(calc[7])
#
#time = ( numpy.arange(0, float(len(window)), 1) / Trois[0] ) * 1000
#over = hamming(len(window)) * 100
#hammingappli = window * hamming(len(window))
#
#fig_size = plt.rcParams["figure.figsize"]
#fig_size[0] = 16
#fig_size[1] = 9
#plt.rcParams["figure.figsize"] = fig_size
#
## Premier graphe : hamming et fenêtre d'origine
#ax1 = fig.add_subplot(gs[0,0])
#ax1.set_xlim([-5,30])
#
#ax1.set_title("Entrée E[n] et fenêtrage F[n]")
#plt.ylabel('Intensité (en %)')
#plt.xlabel('Temps (ms)')
#
#ax1.plot(time, window, 'r-')
#ax1.plot(time, over, 'b-', linewidth=2.0)
#plt.axhline(y=0, xmin=0, xmax=0.145, color = 'red')
#plt.axhline(y=0, xmin=0.855, xmax=1, color = 'red')
#plt.axvline(x=25, ymin=0.5, ymax=0.62, color = 'red')
#
## Second graphe : fenêtre après application de Hamming
#ax2 = fig.add_subplot(gs[0,1], sharex=ax1, sharey=ax1)
#ax2.set_xlim([-5,30])
#
#ax2.set_title("Sortie S[n] = E[n]*F[n]")
#ax2.axes.get_yaxis().set_visible(False)
#plt.xlabel('Temps (ms)')
#
#ax2.plot(time, hammingappli, 'r-')
#plt.axhline(y=0, xmin=0, xmax=0.145, color = 'red')
#plt.axhline(y=0, xmin=0.855, xmax=1, color = 'red')
#plt.axvline(x=25, ymin=0.5, ymax=0.505, color = 'red')
#
#savefig('foo.png')




# HERTZ TO MEL

#hertz = np.linspace(0,10000)
#mel = mfcc.hertz2mel(hertz)
#
#plt.title("Graphique de correspondance entre Hertz et Mels")
#plt.xlabel('Hertz')
#plt.ylabel('Mels')
#
#plt.plot(hertz, mel, c='#000000', linewidth=3.0)
#
#ax = plt.gca()
#ax.grid(True)
#ax.set_ylim([0,3200])
#
#arra = [2000, 4000, 8000]
#for num in arra:
#    plt.axvline(x=num, ymin=0, ymax=mfcc.hertz2mel(num)/ax.get_ylim()[1], linewidth=2, color = 'k')
#    plt.axhline(y=mfcc.hertz2mel(num), xmin=0, xmax=num/max(hertz), linewidth=2, color = 'k')
#    
#    
#print(mfcc.hertz2mel(8000) / mfcc.hertz2mel(4000))
#print(mfcc.hertz2mel(4000) / mfcc.hertz2mel(2000))
#
#savefig('foo.png')




# FFT
 
#rate1, data1 = wav.read("Enregistrements/Demos/Trois/demoTrois24.wav")
#rate2, data2 = wav.read("Enregistrements/Demos/Neuf/demoNeuf15.wav")
#
#data1 = data1[27000:37500]
#data2 = data2[21800:34500]
#
#window1 = mfcc.decoupageSignal(data1, 0.025*rate1, 0.01*rate1)[7]
#window2 = mfcc.decoupageSignal(data1, 0.025*rate1, 0.01*rate1)[10]
#
#fft_out1 = np.fft.fft(window1)
#freqs1 = np.fft.fftfreq(len(window1))
#freq_in_hertz1 = abs(freqs1 * rate1)
#
#fft_out2 = np.fft.fft(window2)
#freqs2 = np.fft.fftfreq(len(window2))
#freq_in_hertz2 = abs(freqs2 * rate2)
#
#ax = plt.gca()
#ax.grid(True)
#ax.set_xlim([0,10000])
#
#plt.title('Exemple de FFT sur la première fenêtre significative')
#plt.ylabel('Amplitude')
#plt.xlabel('Fréquence (Hertz)')
#
#fft_out1 = np.abs(fft_out1) / max(np.abs(fft_out1))
#fft_out2 = np.abs(fft_out2) / max(np.abs(fft_out2))
#
#plt.plot(freq_in_hertz1, fft_out1, color="red", label='Trois')
#plt.plot(freq_in_hertz1, fft_out2, color="blue", label='Neuf')
#plt.legend()
#plt.show()
#
#savefig('foo.png')




# FFT IN MEL

#rate1, data1 = wav.read("Enregistrements/Demos/Trois/demoTrois24.wav")
#rate2, data2 = wav.read("Enregistrements/Demos/Neuf/demoNeuf15.wav")
#
#data1 = data1[27000:37500]
#data2 = data2[21800:34500]
#
#window1 = traitementsignal.framesig(data1, 0.025*rate1, 0.01*rate1)[7]
#window2 = traitementsignal.framesig(data1, 0.025*rate1, 0.01*rate1)[10]
#
#fft_out1 = np.fft.fft(window1)
#freqs1 = np.fft.fftfreq(len(window1))
#freq_in_hertz1 = abs(freqs1 * rate1)
#
#fft_out2 = np.fft.fft(window2)
#freqs2 = np.fft.fftfreq(len(window2))
#freq_in_hertz2 = abs(freqs2 * rate2)
#
#ax = plt.gca()
#ax.grid(True)
#ax.set_xlim([0,4000])
#
#hertz = np.linspace(0,10000)
#mel = mfcc.hertz2mel(hertz)
#
#plt.title('Exemple de FFT en Mel sur la première fenêtre significative')
#plt.ylabel('Amplitude')
#plt.xlabel('Fréquence (Mels)')
#
#fft_out1 = np.abs(fft_out1) / max(np.abs(fft_out1))
#fft_out2 = np.abs(fft_out2) / max(np.abs(fft_out2))
#
#l1 = mfcc.hertz2mel(freq_in_hertz1)
#l2 = mfcc.hertz2mel(freq_in_hertz2)
#
#plt.plot(l1, fft_out1, color="red", label='Trois')
#plt.plot(l2, fft_out2, color="blue", label='Neuf')
##plt.plot(freq_in_hertz1, fft_out1, color="red", label='Trois')
##plt.plot(freq_in_hertz1, fft_out2, color="blue", label='Neuf')
#plt.legend()
#plt.show()
#
#savefig('foo.png')



# BANC MELS 

#ax = plt.gca()
#ax.set_xlim([0,25000])
##ax.set_ylim([0.1,0.65])
#
#t = mfcc.get_filterbanks(10)
#for l in t:
#    print(len(l))
##    print(l)
#    
#for i in range(len(t)):
#    if i != 6:
#        t[i] = [0 for a in range(len(t[i]))]
#    
#plt.title('Banc de filtres mels : un filtre représente un coefficient (ici, 10)')
#plt.xlabel('Fréquence en Hertz')
#plt.ylabel('Amplitude')
#
#x = 100*np.arange(len(t[0]))
#
#for i in range(len(t)):
#    plot(x, mfcc.hertz2mel(t[i]))
#    
#savefig('graph_mel.png')




# FFT LIMITEE A UN FILTRE

#filtre = mfcc.get_filterbanks(26, 2046)
#    
#for i in range(len(filtre)):
#    if i != 9:
#        filtre[i] = [0 for a in range(len(filtre[i]))]
# 
#rate, data = wav.read("Enregistrements/Demos/Trois/demoTrois24.wav")
#data = data[27000:37500]
#
#window = traitementsignal.framesig(data, 0.025*rate, 0.01*rate)[7]
#
#fft_out = np.fft.fft(window, 1024)
#freqs = np.fft.fftfreq(len(fft_out))
#freq_in_hertz = abs(freqs * rate)
#
#ax = plt.gca()
#ax.grid(True)
#ax.set_xlim([0,10000])
#ax.set_ylim([0.003,1])
#
#plt.ylabel('Amplitude')
#plt.xlabel('Fréquence (Hertz)')
#
#fft_out = np.abs(fft_out) / max(np.abs(fft_out))
#
#filtrage = filtre[9]*fft_out1
#
#"""Première étape"""
##plt.title('FFT de la première fenêtre du mot "Trois" avant filtrage')
##ax.text(4100, 0.75, 'Filtre n°8', fontsize=13, fontweight='bold')
##plt.plot(freq_in_hertz, fft_out, color="b", linewidth=2.0, label='Trois')
##plt.plot(freq_in_hertz, filtre[9], color="black", linewidth=2.0, linestyle='--', label='Neuf')
#
#"""Seconde étape"""
#plt.title('FFT après application du filtre n°8')
#ax.text(4100, 0.75, 'Filtre n°8', fontsize=13, fontweight='bold')
#plt.plot(freq_in_hertz, fft_out, color="b", linestyle=':', label='Trois')
#plt.plot(freq_in_hertz, filtrage, color="red", linewidth=2.0, label='Trois')
#plt.plot(freq_in_hertz, filtre[9], color="black", linewidth=1.0, label='Neuf')
#
##plt.legend()
#plt.show()
#
#savefig('foo.png')



# DTW ILLUSTRATIONS

#def deb_fin(sig):
#    for i,a in enumerate(sig):
#        if a != 0:
#            i_deb = i
#            break
#    for i,a in enumerate(sig[i_deb:]):
#        if a != 0:
#            i_fin = i_deb+i
#    return i_deb, i_fin
#    
#y = [0,0,0,0]
#y_file = "Enregistrements/Chiffres/Un2.wav"
#rate, y[0] = wav.read(y_file)[0], wav.read(y_file)[1]
#deb, fin = deb_fin(y[0])
#y[1] = y[0][deb:fin+1]
#y[3] = mfcc.mfcc(y[1],rate)
#
#z = [0,0,0,0]
#z_file = "Enregistrements/Chiffres/Un3.wav"
#rate, z[0] = wav.read(z_file)[0], wav.read(z_file)[1]
#deb, fin = deb_fin(z[0])
#z[1] = z[0][deb:fin+1]
#z[3] = mfcc.mfcc(z[1],rate)
#
#x = [0,0,0,0]
#x_file = "Enregistrements/Chiffres/Quatre2.wav"
#rate, x[0] = wav.read(x_file)[0], wav.read(x_file)[1]
#deb, fin = deb_fin(x[0])
#x[1] = x[0][deb:fin+1]
#x[3] = mfcc.mfcc(x[1],rate)

#dtwcalc = dtw.dtw(x[3], z[3])
#dtwcalc = dtw.dtw(x[3], y[3])
#
#fig = plt.figure()
#
#axes = plt.gca()
#axes.grid(True)
#axes.imshow(dtwcalc[1], interpolation='nearest', cmap='YlOrRd')
#axes.plot(dtwcalc[2][1], dtwcalc[2][0], color='black', marker='', linewidth=3.0)
#axes.set_xlim([0.5, max(dtwcalc[2][1])+0.5])
#axes.set_ylim([-0.5, max(dtwcalc[2][0])+0.5])
#plt.ylabel('Échantillons pour le mot "Un" (Fichier 1)')

#plt.xlabel('Échantillons pour le mot "Un" (Fichier 2)')
#axes.set_title('Entre deux fichiers sons distincts pour le mot "Un"')

#plt.xlabel('Échantillons pour le mot "Quatre"')
#axes.set_title('Entre le mot "Un" et le mot "Quatre"')
#
#savefig('hey.png')




# ILLUSTRATION DU RATTACHEMENT TEMPOREL (DTW)

#def deb_fin(sig):
#    for i,a in enumerate(sig):
#        if a != 0:
#            i_deb = i
#            break
#    for i,a in enumerate(sig[i_deb:]):
#        if a != 0:
#            i_fin = i_deb+i
#    return i_deb, i_fin
#    
#y = [0,0,0,0]
#y_file = "Enregistrements/Chiffres/Un2.wav"
#rate, y[0] = wav.read(y_file)[0], wav.read(y_file)[1]
#deb, fin = deb_fin(y[0])
#y[1] = y[0][deb:fin+1]
#y[1] = y[1][5000:12000]
#y[3] = mfcc.mfcc(y[1], rate)
#
#x = [0,0,0,0]
#x_file = "Enregistrements/Chiffres/Un3.wav"
#rate, x[0] = wav.read(x_file)[0], wav.read(x_file)[1]
#deb, fin = deb_fin(x[0])
#x[1] = x[0][deb:fin+1]
#x[1] = x[1][5000:12000]
#x[3] = mfcc.mfcc(x[1], rate)
#
#dtwcalc2 = dtw.dtw(x[3], y[3])
#print(dtwcalc2[2])
#
#axes = plt.gca()
#axes.set_xlim([0,len(dtwcalc2[2][0]) - 1])
#axes.set_ylim([-750,750])
#
#t = [range(0, len(dtwcalc2[2][0])), range(0, len(dtwcalc2[2][0]))]
#print(t)
#
#plt.plot(x[1], 'r-', linewidth=2.0, label = 'Premier fichier')
#plt.plot(y[1], 'b-', linewidth=2.0, label = 'Deuxième fichier')
#plt.xlabel('Échantillons')
#plt.ylabel('Amplitude')
#plt.legend()
#axes.set_title('Comparaison des signaux AVEC alignement temporel')
#
#for [map_x, map_y] in np.nditer(dtwcalc2[2]):
#    plt.plot([map_x, map_y], [x[1][map_x], y[1][map_y]], marker='o', color='black', linewidth=2.0)    
#
##for [a, b] in np.nditer(t):
##    plt.plot([a, b], [x[1][a], y[1][b]], marker='o', color='black', linewidth=2.0)    
#    
#savefig('hey.png')
    
    
    
    
# VARIABILITE SIGNAL

fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 10
fig_size[1] = 12
plt.rcParams["figure.figsize"] = fig_size

gs = gridspec.GridSpec(3, 1)
fig = plt.figure()

rate1,son1 = wav.read("Enregistrements/Chiffres/Cinq4.wav")
deb1,fin1 = deb_fin(son1)

rate2,son2 = wav.read("Enregistrements/Chiffres/Cinq5.wav")
deb2,fin2 = deb_fin(son2)

rate3,son3 = wav.read("Enregistrements/Chiffres/Cinq1.wav")
deb3,fin3 = deb_fin(son3)

signal1 = np.array(son1[deb1:fin1], dtype=float)*100 / max(son1[deb1:fin1])
signal2 = np.array(son2[deb2:fin2], dtype=float)*100 / max(son2[deb2:fin2])
signal3 = np.array(son3[deb3:fin3], dtype=float)*100 / max(son3[deb3:fin3])

time1 = ( numpy.arange(0, float(len(signal1)), 1) / rate1 ) * 1000
time2 = ( numpy.arange(0, float(len(signal2)), 1) / rate2 ) * 1000
time3 = ( numpy.arange(0, float(len(signal3)), 1) / rate3 ) * 1000

ax1 = fig.add_subplot(gs[0,0])
ax1.axes.get_yaxis().set_visible(False)
ax1.plot(time1, signal1, 'r-')
ax1.set_title('Variabilité pour des enregistrements du mot "Cinq"')

ax2 = fig.add_subplot(gs[1,0], sharex=ax1)
ax2.axes.get_yaxis().set_visible(False)
ax2.plot(time2, signal2, 'b-')

ax3 = fig.add_subplot(gs[2,0], sharex=ax1)
ax3.axes.get_yaxis().set_visible(False)
ax3.plot(time3, signal3, 'g-')

plt.xlabel('Temps')
plt.legend()

savefig('foo.png')




# VITESSE ALGORITHME


#emp = 0.00

#L = []
#for nb_fichiers in range(0,40):
#    t1 = time.clock()
#    
#    lexique = ["Un", "Deux", "Trois", "Quatre", "Cinq", "Six", "Sept", "Huit", "Neuf"]
#    rate = wav.read("Enregistrements/Chiffres/Un1.wav")[0]
#    
#    Mots = OrderedDict()
#    Tests = OrderedDict()
#    Reconnus = OrderedDict()
#    for mot in lexique:
#        Mots[mot] = [[0, 0] for i in range(nb_fichiers)]
#        Tests[mot] = [[0, 0] for i in range(nb_tests)]
#            
#    for mot in lexique:
#        files = [f for f in listdir("Enregistrements/Demos/"+mot+"/") if isfile(join("Enregistrements/Demos/"+mot+"/", f))][0:nb_fichiers]
#        for i, sound in enumerate(files):
#            Mots[mot][i][0] = numpy.trim_zeros( numpy.asarray( wav.read("Enregistrements/Demos/"+mot+"/"+sound)[1] ) )
#            Mots[mot][i][1] = mfcc.mfcc(Mots[mot][i][0], rate, preemph = emp)
#            
#    mot = "Quatre"
#    test = numpy.trim_zeros( numpy.asarray( wav.read("Enregistrements/Demos/Deux/demoDeux4.wav")[1] ) )
#    test_mfcc = mfcc.mfcc(test, rate, preemph = emp)
#            
#    for mot_test in lexique:
#        for i in range(nb_fichiers):
#            print(i, mot_test)
#            dtw.dtw( test_mfcc, Mots[mot_test][i][1] )[0]
#    
#    t2 = time.clock()
#    
#    L.append(t2 - t1)

#A = range(0,40)
#L = [0.08993147033743298, 0.6878673694757254, 1.323066072139909, 1.9265075018265634, 2.4862908600814535, 3.0832141778075766, 3.6651402571194467, 4.279853510870225, 4.903951344773532, 5.432616668174205, 6.725625077378936, 6.660626139033866, 7.663760747251217, 8.097618364897471, 8.353247541354904, 8.956999448965917, 9.8322238879407, 10.843314813710094, 11.44874548315579, 12.421085684850595, 12.589530864007429, 12.289416423383955, 13.051081957832594, 13.451911524055276, 14.145483925390181, 14.793519671406557, 15.534229870414038, 16.357452785224268, 17.122759378967203, 17.040305038146016, 17.781543819404305, 18.220558000675737, 19.378673694758163, 18.772143522910937, 19.342161747451883, 20.114736026408536, 22.082959188190216, 23.079895784620476, 21.923098659492098, 23.6977003464649]
#
#tes = stats.linregress(A,L)
#print(tes)
#
#y = [x*tes[0] for x in A]
#
#fig_size = plt.rcParams["figure.figsize"]
#fig_size[0] = 10
#fig_size[1] = 6
#plt.rcParams["figure.figsize"] = fig_size
#
#plt.plot(y, linestyle="--", color='red', label='Évolution linéaire')
#plt.plot(L, linewidth=2.0, color='black', label='Évolution expérimentale')
#plt.ylabel("Temps d'exécution (en secondes)")
#plt.xlabel("Nombre de fichiers")
#plt.title("Représentation du temps de calcul en fonction du nombre de fichiers")
#plt.legend()
#
#savefig('foo.png')