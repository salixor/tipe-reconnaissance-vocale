# -*- coding: utf-8 -*-

import numpy
import math
import traitementsignal
from scipy.fftpack import dct



"""Convertit une valeur en Hertz vers des Mels.

:param hz: Une valeur ou un array numpy en Hertz.
:retour: Une valeur ou un array numpy en Mels.
"""
def hertz2mel(hz):
    return 2595 * numpy.log10(1 + hz/700.0)



"""Convertit une valeur en Mels vers des Hertz.

:param mel: Une valeur ou un array numpy en Mels.
:retour: Une valeur ou un array numpy en Hertz.
"""
def mel2hertz(mel):
    return 700 * ( 10**(mel/2595.0) - 1)



"""Calcule un banc de filtres Mels.
Les filtres sont stockés dans les lignes.
Les colonnes correspondent aux fréquences de la fft.

:param nfilt: Le nombre de filtres dans la banque de filtres, défini par défaut à 26.
:param nfft: La taille de la FFT, définie par défaut à 512.
:param samplerate: La fréquence d'échantillonnage du signal audio d'entrée. A un effet sur l'espacement en Mels.
:param lowfreq: La plus petite fréquence des filtres mel en Hertz, définie par défaut à 0.
:param highfreq: La plus grande fréquence des filtres mel en Hertz, définie par défaut à samplerate/2.

:retour: Un array numpy de dimension (nfilt * (nfft/2 + 1)) contenant des bancs de filtre Mels.
"""
def get_filterbanks(nfilt = 26, nfft = 512, samplerate = 44100, lowfreq = 0, highfreq = None):
    highfreq = highfreq or samplerate/2
    assert highfreq <= samplerate / 2
    
    # Calcul des points équitablement séparés en échelle Mels
    lowmel = hertz2mel(lowfreq)
    highmel = hertz2mel(highfreq)
    melpoints = numpy.linspace(lowmel, highmel, nfilt+2)
    
    # Nos points sont en Hertz, mais on utilise des bins fft : il faut passer des Hertz à l'équivalent en numéro de bin fft
    bin = numpy.floor( (nfft+1) * mel2hertz(melpoints) / samplerate)

    fbank = numpy.zeros([nfilt,nfft/2+1])
    for j in xrange(0,nfilt):
        for i in xrange(int(bin[j]), int(bin[j+1])):
            fbank[j,i] = (i - bin[j]) / (bin[j+1] - bin[j])
        for i in xrange(int(bin[j+1]), int(bin[j+2])):
            fbank[j,i] = (bin[j+2] - i) / (bin[j+2] - bin[j+1])
    
    return fbank     



"""Apllique un lifter cepstral à la matrice des cepstres. Ceci augmente la magnitude des coefficients de DTC haute fréquence.

:param cepstra: La matrice des cepstres en mel, de dimension (numframes * numcep).
:param L: Le coefficient de lifter, défini par défaut à 22.
"""
def lifter(cepstra, L = 22):
    if L < 0:
        return cepstra
        
    nframes, ncoeff = numpy.shape(cepstra)
    n = numpy.arange(ncoeff)
    lift = 1 + (L/2) * numpy.sin(numpy.pi*n / L)
    
    return lift*cepstra



"""Applique les filtres Mel au spectre de puissance et somme l'énergie dans chacun des filtres.
Calcule donc les paramètres énergétiques du signal.

:param signal: Le signal audio (sous forme d'un tableau de dimension N*1) duquel on va extraire les paramètres.
:param samplerate: La fréquence d'échantillonnage du signal audio d'entrée.
:param winlen: La dimension d'une fenêtre d'analyse en secondes, définie par défaut à 25ms.
:param winstep: Le pas entre deux fenêtres successives en secondes, défini par défaut à 10ms.
:param nfilt: Le nombre de filtres dans la banque de filtres, défini par défaut à 26.
:param nfft: La taille de la FFT, définie par défaut à 512.
:param lowfreq: La plus petite fréquence des filtres mel en Hertz, définie par défaut à 0.
:param highfreq: La plus grande fréquence des filtres mel en Hertz, définie par défaut à samplerate/2.
:param preemph: Application d'un filtre de préamplification, défini par défaut à 0.97.

:retour: - Un array numpy de dimension (nbfenetres * nfilt) contenant les paramètres, chaque ligne contenant un vecteur accoustique.
         - La valeur de l'énergie dans chaque fenêtre (énergie totale, non fenêtrée)
"""
def fbank(signal, samplerate = 44100, winlen = 0.025, winstep = 0.01,
          nfilt = 26, nfft = 512, lowfreq = 0, highfreq = None, preemph = 0.97):       
    highfreq = highfreq or samplerate/2
    
    signal = traitementsignal.preemphasis(signal, preemph)
    fenetres = traitementsignal.framesig(signal, winlen*samplerate, winstep*samplerate)
    
    powerspectrum = traitementsignal.powspec(fenetres, nfft)
    energie = numpy.sum(powerspectrum, 1) # Stocke l'énergie totale dans chaque fenêtre
    energie = numpy.where(energie == 0, numpy.finfo(float).eps, energie) # Si l'énergie est nulle, on met un epsilon pour le log
        
    fb = get_filterbanks(nfilt, nfft, samplerate, lowfreq, highfreq)
    coeff = numpy.dot(powerspectrum, fb.T) # Calcule les énergies de la banque de filtres
    coeff = numpy.where(coeff == 0, numpy.finfo(float).eps, coeff) # Si le paramètre est nul, on met un epsilon pour le log
    
    return coeff, energie



"""Applique les filtres Mel au spectre de puissance et somme l'énergie dans chacun des filtres.
Calcule donc les paramètres énergétiques du signal, sous forme de logarithme.

:param signal: Le signal audio (sous forme d'un tableau de dimension N*1) duquel on va extraire les paramètres.
:param samplerate: La fréquence d'échantillonnage du signal audio d'entrée.
:param winlen: La dimension d'une fenêtre d'analyse en secondes, définie par défaut à 25ms.
:param winstep: Le pas entre deux fenêtres successives en secondes, défini par défaut à 10ms.
:param nfilt: Le nombre de filtres dans la banque de filtres, défini par défaut à 26.
:param nfft: La taille de la FFT, définie par défaut à 512.
:param lowfreq: La plus petite fréquence des filtres mel en Hertz, définie par défaut à 0.
:param highfreq: La plus grande fréquence des filtres mel en Hertz, définie par défaut à samplerate/2.
:param preemph: Application d'un filtre de préamplification, défini par défaut à 0.97.

:retour: Un array numpy de dimension (nbfenetres * nfilt) contenant les paramètres, chaque ligne contenant un vecteur accoustique.
"""
def logfbank(signal, samplerate = 44100, winlen = 0.025, winstep = 0.01,
             nfilt = 26, nfft = 512, lowfreq = 0, highfreq = None, preemph = 0.97):
    coeff, energie = fbank(signal, samplerate, winlen, winstep, nfilt, nfft, lowfreq, highfreq, preemph)
    
    return numpy.log(coeff)



"""Calcule les coefficients MFCC d'un signal.

:param signal: Le signal audio (sous forme d'un tableau de dimension N*1) duquel on va extraire les paramètres.
:param samplerate: La fréquence d'échantillonnage du signal audio d'entrée.
:param winlen: La dimension d'une fenêtre d'analyse en secondes, définie par défaut à 25ms.
:param winstep: Le pas entre deux fenêtres successives en secondes, défini par défaut à 10ms.
:param numcep: Le nombre de cepstres à renvoyer, défini par défaut à 13.
:param nfilt: Le nombre de filtres dans la banque de filtres, défini par défaut à 26.
:param nfft: La taille de la FFT, définie par défaut à 512.
:param lowfreq: La plus petite fréquence des filtres mel en Hertz, définie par défaut à 0.
:param highfreq: La plus grande fréquence des filtres mel en Hertz, définie par défaut à samplerate/2.
:param preemph: Application d'un filtre de préamplification, défini par défaut à 0.97.
:param ceplifter: Application d'un lifter aux coefficients, défini par défaut à 22.
:param appendEnergy: Si cette variable est définie à True, le coefficient cepstral zéro est remplacé par le log de l'énergie totale.

:retour: Un array numpy de dimension (nbfenetres * numcep) contenant les paramètres, chaque ligne contenant un vecteur accoustique.
"""
def mfcc(signal, samplerate = 44100, winlen = 0.025, winstep = 0.01, numcep = 13,
         nfilt = 26, nfft = 512, lowfreq = 0, highfreq = None, preemph = 0.97, ceplifter = 22, appendEnergy = True):
    coeff, energie = fbank(signal, samplerate, winlen, winstep, nfilt, nfft, lowfreq, highfreq, preemph)
    
    coeff = numpy.log(coeff)
    coeff = dct(coeff, type = 2, axis = 1, norm='ortho')[:,:numcep]
    coeff = lifter(coeff, ceplifter)
    
    if appendEnergy:
        coeff[:,0] = numpy.log(energie) # Remplace le premier coefficient cepstral par le log de l'énergie totale
        
    return coeff