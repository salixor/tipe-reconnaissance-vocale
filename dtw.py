# -*- coding: utf-8 -*-

import numpy as np
from numpy import array, zeros, argmin, inf
from numpy.linalg import norm



"""Exécute l'algorithme de DTW sur deux séquences.

:param x: Array numpy de dimension N1 * M.
:param y: Array numpy de dimension N2 * M.
:param dist: La norme utilisée pour le calcul de distance (par défaut, la norme L1).

:retour: La distance minimale, la matrice du coût accumulé et le chemin suivi.
"""
def dtw(x, y, dist = lambda x, y: norm(x - y, ord=1)):
    x = array(x)
    if len(x.shape) == 1:
        x = x.reshape(-1, 1)
    
    y = array(y)
    if len(y.shape) == 1:
        y = y.reshape(-1, 1)

    r, c = len(x), len(y)

    D = zeros((r + 1, c + 1))
    D[0, 1:] = inf
    D[1:, 0] = inf

    for i in range(r):
        for j in range(c):
            D[i+1, j+1] = dist(x[i], y[j])

    for i in range(r):
        for j in range(c):
            D[i+1, j+1] += min(D[i, j], D[i, j+1], D[i+1, j])

    D = D[1:, 1:]

    dist = D[-1, -1] / sum(D.shape)

    return dist, D, trackeback(D)



"""Permet de retrouver le chemin suivi par l'algorithme de DTW.

:param D: La matrice du coût accumulé.

:retour: Le chemin suivi sur les abscisses et les ordonnées.
"""
def trackeback(D):
    i, j = array(D.shape) - 1
    p, q = [i], [j]
    while (i > 0 and j > 0):
        tb = argmin((D[i-1, j-1], D[i-1, j], D[i, j-1]))

        if (tb == 0):
            i = i - 1
            j = j - 1
        elif (tb == 1):
            i = i - 1
        elif (tb == 2):
            j = j - 1

        p.insert(0, i)
        q.insert(0, j)

    p.insert(0, 0)
    q.insert(0, 0)
    return (array(p), array(q))