# -*- coding: utf-8 -*-

import dtw
import mfcc
import audio_record
import numpy
import time
from collections import OrderedDict

from pylab import *
from matplotlib import gridspec

from os import listdir
from os.path import isfile, join

from scipy.io import wavfile as wav
   

# Enregistrement des coefficients cepstraux des fichiers sons

t1 = time.clock()

emp = 0.00

nb_fichiers = 1
nb_tests = 100
lexique = ["Un", "Deux", "Trois", "Quatre", "Cinq", "Six", "Sept", "Huit", "Neuf"]
rate = wav.read("Enregistrements/Chiffres/Un1.wav")[0]

Mots = OrderedDict()
Tests = OrderedDict()
Reconnus = OrderedDict()
for mot in lexique:
    Mots[mot] = [[0, 0] for i in range(nb_fichiers)]
    Tests[mot] = [[0, 0] for i in range(nb_tests)]
    Reconnus[mot] = 0

for i in range(nb_fichiers):
    for mot in lexique:
        Mots[mot][i][0] = numpy.trim_zeros( numpy.asarray( wav.read("Enregistrements/Chiffres/"+mot+str(i+1)+".wav")[1] ) )
        Mots[mot][i][1] = mfcc.mfcc(Mots[mot][i][0], rate, preemph = emp)
        
for mot in lexique:
    files = [f for f in listdir("Enregistrements/Demos/"+mot+"/") if isfile(join("Enregistrements/Demos/"+mot+"/", f))][0:nb_tests]
#    for i in range(nb_tests):
    for i, sound in enumerate(files):
#        Tests[mot][i][0] = numpy.trim_zeros( numpy.asarray( wav.read("Enregistrements/Demos/"+mot+"/demo"+mot+str(i+1)+".wav")[1] ) )
        Tests[mot][i][0] = numpy.trim_zeros( numpy.asarray( wav.read("Enregistrements/Demos/"+mot+"/"+sound)[1] ) )
        Tests[mot][i][1] = mfcc.mfcc(Tests[mot][i][0], rate, preemph = emp)
        
for mot_test in lexique:
    for j in range(nb_tests):    
        print(mot_test,j)
        Distances = OrderedDict()
        Moyennes = OrderedDict()
        for mot in lexique:
            Distances[mot] = []
            Moyennes[mot] = 0
        
        # Enregistrement du fichier audio
#        print("...")
#        audio_record.record_to_file('Enregistrements/demo.wav')
#        sig = numpy.trim_zeros( wav.read("Enregistrements/demo.wav")[1] )
        
        # Calculs des coefficients cepstraux du fichier enregistré et DTW
#        mfcc_voix = mfcc.mfcc(sig,rate)
        for mot in lexique:
            for i in range(nb_fichiers):
                Distances[mot].append( dtw.dtw( Tests[mot_test][j][1], Mots[mot][i][1] )[0] )
                
        # Calcul des moyennes
        for mot in lexique:
            for i in range(nb_fichiers):
                Moyennes[mot] += Distances[mot][i]
            Moyennes[mot] = Moyennes[mot] / nb_fichiers
        
        moy_min = Moyennes["Un"]
        mot_min = "Un"
        for mot, moy in Moyennes.items():
            if moy < moy_min:
                moy_min = moy
                mot_min = mot
                
        if mot_test == mot_min:
            Reconnus[mot_test] += 1

print(Reconnus)

ReconnusMoyenne = []
for val in Reconnus.values():
    ReconnusMoyenne.append(val*100/30)
    
    
t2 = time.clock()

print(t2-t1)


# RESULTATS TRAITES


#ax = plt.gca()
#
#deb = 1
#fin = 7

#a = OrderedDict([('Un', 21), ('Deux', 30), ('Trois', 30), ('Quatre', 25), ('Cinq', 16), ('Six', 24), ('Sept', 22), ('Huit', 12), ('Neuf', 27)])
#b = OrderedDict([('Un', 20), ('Deux', 30), ('Trois', 30), ('Quatre', 28), ('Cinq', 12), ('Six', 17), ('Sept', 21), ('Huit', 13), ('Neuf', 26)])
#c = OrderedDict([('Un', 19), ('Deux', 30), ('Trois', 30), ('Quatre', 30), ('Cinq', 11), ('Six', 11), ('Sept', 16), ('Huit', 12), ('Neuf', 26)])
#d = OrderedDict([('Un', 18), ('Deux', 29), ('Trois', 29), ('Quatre', 29), ('Cinq', 6), ('Six', 23), ('Sept', 17), ('Huit', 11), ('Neuf', 28)])
#e = OrderedDict([('Un', 19), ('Deux', 28), ('Trois', 30), ('Quatre', 15), ('Cinq', 10), ('Six', 25), ('Sept', 20), ('Huit', 12), ('Neuf', 16)])


#
#a = OrderedDict([('Un', 50), ('Deux', 95), ('Trois', 78), ('Quatre', 24), ('Cinq', 93), ('Six', 49), ('Sept', 78), ('Huit', 37), ('Neuf', 100)])
#b = OrderedDict([('Un', 59), ('Deux', 95), ('Trois', 91), ('Quatre', 52), ('Cinq', 84), ('Six', 53), ('Sept', 100), ('Huit', 42), ('Neuf', 100)])
#c = OrderedDict([('Un', 63), ('Deux', 95), ('Trois', 100), ('Quatre', 67), ('Cinq', 72), ('Six', 87), ('Sept', 100), ('Huit', 51), ('Neuf', 100)])
#d = OrderedDict([('Un', 64), ('Deux', 95), ('Trois', 100), ('Quatre', 87), ('Cinq', 95), ('Six', 94), ('Sept', 100), ('Huit', 58), ('Neuf', 100)])
#e = OrderedDict([('Un', 63), ('Deux', 95), ('Trois', 100), ('Quatre', 88), ('Cinq', 95), ('Six', 93), ('Sept', 100), ('Huit', 67), ('Neuf', 100)])
#
#
#amoy = []
#for val in a.values():
#    amoy.append(val)
#bmoy = []
#for val in b.values():
#    bmoy.append(val)
#cmoy = []
#for val in c.values():
#    cmoy.append(val)
#dmoy = []
#for val in d.values():
#    dmoy.append(val)
#emoy = []
#for val in e.values():
#    emoy.append(val)
#
#x = np.arange(fin-deb)
#w = 0.14
#d = 0.02
#
#plt.xticks(x+3*w, a.keys()[deb:fin])
#
#fig_size = plt.rcParams["figure.figsize"]
#fig_size[0] = 15
#fig_size[1] = 6
#plt.rcParams["figure.figsize"] = fig_size
#
#rects1 = ax.bar(x, amoy[deb:fin], width = w, color='black')
#rects2 = ax.bar(x + w + 1*d, bmoy[deb:fin], width = w, color='red')
#rects3 = ax.bar(x + 2*w + 2*d, cmoy[deb:fin], width = w, color='yellow')
#rects4 = ax.bar(x + 3*w + 3*d, dmoy[deb:fin], width = w, color='green')
#rects5 = ax.bar(x + 4*w + 4*d, emoy[deb:fin], width = w, color='blue')
#
#ax.legend( (rects1[0], rects2[0], rects3[0], rects4[0], rects5[0]),
#           ('1 fichier', '2 fichiers', '3 fichiers', '4 fichiers', '5 fichiers'),
#           prop={'size':12}, loc=4 )
#
#plt.axhline(y=75, xmin=0, xmax=10000, linewidth=1, linestyle='--', color = 'k')
#plt.axhline(y=25, xmin=0, xmax=10000, linewidth=1, linestyle='--', color = 'k')
#plt.axhline(y=50, xmin=0, xmax=10000, linewidth=3, linestyle='--', color = 'k')
#plt.ylabel("Taux de reconnaissance")
#
#savefig('b.png')