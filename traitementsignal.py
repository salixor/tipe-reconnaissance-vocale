# -*- coding: utf-8 -*-

import numpy
import math



"""Découpe le signal en un ensemble de fenêtres qui se chevauchent.

:param signal: Le signal audio (sous forme d'un tableau de dimension N*1) duquel on va extraire les paramètres.
:param winlen: La dimension d'une fenêtre d'analyse en secondes, définie par défaut à 25ms.
:param winstep: Le pas entre deux fenêtres successives en secondes, défini par défaut à 10ms.

:retour: Un array numpy de dimension (nbfenetres * frame_len) qui contient les fenetres.
"""
def framesig(signal, winlen, winstep):
    siglen = len(signal) # Longueur du signal
    winlen = int(round(winlen)) # Longeur de la fenetre
    winstep = int(round(winstep)) # Pas des fenetres
    
    nbfen = 1
    if siglen > winlen:
        nbfen = 1 + int( math.ceil( ( 1.0 * siglen - winlen ) / winstep ) ) #Nombre de fenetres
    
    padlen = int( (nbfen - 1) * winstep + winlen ) # Longueur d'un signal fictif qui correspond au nombre de fenetres
    
    zeros = numpy.zeros( (padlen - siglen) ) # Tableau de 0 qui va remplir ce qui manque au signal
    padsignal = numpy.concatenate( (signal, zeros) ) # Créé un signal fictif de bonne longueur par concaténation
    
    t = numpy.tile( numpy.arange(0, winlen), (nbfen, 1) ) # Tableau de nbfen lignes de 0 à winlen
    t2 = numpy.tile( numpy.arange(0, nbfen * winstep, winstep), (winlen, 1) ).T # nbfen lignes et chaque ligne multiple de 10
    
    indices = t + t2 # Tableau de dimension (nbfen * winlen) contenant les indices
    indices = numpy.array(indices, dtype = numpy.int32) # Modification du type en int
    
    return padsignal[indices]



"""Réalise la préamplification du signal d'entrée.

:param signal: Le signal audio (sous forme d'un tableau de dimension N*1) que l'on veut filtrer.
:param coeff: Le coefficient de préamplification, défini par défaut à 0.97.

:retour: Le signal filtré.
"""
def preemphasis(signal, coeff = 0.95):
    return numpy.append(signal[0], signal[1:] - coeff * signal[:-1])



"""Calcule le spectre en amplitude de chaque fenêtres du tableau frames.
Si le tableau frames est de dimension (N * D), le tableau de sortie sera de dimension (N * NFFT).

:param frames: Le tableau des fenêtres où chaque ligne est une fenêtre.
:param NFFT: La taille de la FFT. Si NFFT > winlen, les fenêtres sont complétées par des zéros.

:retour: Un tableau de dimension (N * FFT) où chaque ligne est le spectre en amplitude de la fenêtre associée.
"""
def magspec(frames, NFFT):
    complex_spec = numpy.fft.rfft(frames, NFFT)
    return numpy.absolute(complex_spec)
    


"""Calcule le spectre en puissance de chaque fenêtres du tableau frames.
Si le tableau frames est de dimension (N * D), le tableau de sortie sera de dimension (N * NFFT).

:param frames: Le tableau des fenêtres où chaque ligne est une fenêtre.
:param NFFT: La taille de la FFT. Si NFFT > winlen, les fenêtres sont complétées par des zéros.

:retour: Un tableau de dimension (N * FFT) où chaque ligne est le spectre en puissance de la fenêtre associée.
"""
def powspec(frames, NFFT):
    return 1.0 / NFFT * numpy.square( magspec(frames,NFFT) )   